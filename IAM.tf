resource "aws_iam_role" "lambda_role" {
  name               = "Spacelift_Test_Lambda_Function_Role"
  assume_role_policy = <<EOF
{
 "Version": "2012-10-17",
 "Statement": [
   {
     "Action": "sts:AssumeRole",
     "Principal": {
       "Service": "lambda.amazonaws.com"
     },
     "Effect": "Allow",
     "Sid": ""
   }
 ]
}
EOF
}

resource "aws_iam_role_policy" "s3_bucket_policy" {
  name = "s3_bucket_policy_for_IAM_user"
  role = aws_iam_role.lambda_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:AbortMultipartUpload",
                "s3:Get*",
                "s3:List*",
                "s3:Delete*",
                "s3:PutObject*"],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy" "dynamodb_read_log_policy" {
  name = "lambda-dynamodb_read_log_policy"
  role = aws_iam_role.lambda_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "dynamodb: BatchGetItem",
                "dynamodb: BatchWriteItem",
                "dynamodb: GetItem",
                "dynamodb: PutItem",
                "dynamodb: UpdateItem",
                "dynamodb: DeleteItem",
                "dynamodb: GetRecords",
                "dynamodb: Scan",
                "dynamodb: Query",
                "dynamodb: GetShardIterator",
                "dynamodb: DescribeStream",
                "dynamodb: ListStreams"],
            "Resource": "arn:aws:dynamodb:us-west-1:accountID:*"
        }
    ]
}
EOF
}

