data "archive_file" "init" {
  type        = "zip"
  source_file = "src/basiccode.py"
  output_path = "outputs/basiccode.zip"
}


resource "aws_lambda_function" "test_lambda" {
  filename      = "outputs/basiccode.zip"
  function_name = "basiccode"
  role          = aws_iam_role.lambda_role.arn
  handler       = "basiccode.count_row"
  source_code_hash = filebase64sha256("src/basiccode.py")
  runtime = "python3.7"
}

