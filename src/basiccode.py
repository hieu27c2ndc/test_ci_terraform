import boto3
import os
import csv

s3_endpoint = 'http://%s:4566' % os.environ['LOCALSTACK_HOSTNAME']

def count_row(event, context):
    data = read_file_from_s3()
    number_row = count_row_data(data)
    insert_row_to_dynamodb(number_row)


def read_file_from_s3():
    S3_BUCKET = 'test'
    object_key = "test.csv"
    try:
        s3_client = boto3.client(service_name="s3", endpoint_url=s3_endpoint,
                         aws_access_key_id='mock', aws_secret_access_key='mock')
        s3_object = s3_client.get_object(
            Bucket=S3_BUCKET, Key=object_key)
        data = s3_object['Body'].read().decode("utf-8").split("\n")
    except:
        print("error when read data form s3")
    return data
    

def count_row_data(data):
    csv_reader = csv.reader(data, delimiter=',', quotechar="'")
    rows = []
    for i in csv_reader:
        if len(i) != 0:
            rows.append(i)
    number_row = len(rows)
    return number_row


def insert_row_to_dynamodb(number_row):
    try:
        dynamodb = boto3.resource(service_name="dynamodb", endpoint_url=s3_endpoint,
                        aws_access_key_id='mock', aws_secret_access_key='mock')
        table = dynamodb.Table('mytable')
        table.put_item(
            Item={"rows": number_row}
        )
    except:
        print("error when insert to dynamodb")
