# Create Bucket
resource "aws_s3_bucket" "test" {
  bucket        = "test"
  force_destroy = "true"
}

resource "aws_s3_object" "object" {
  bucket        = aws_s3_bucket.test.id
  key           = "test.csv"
  source        = "../test.csv"
  force_destroy = "true"
}
