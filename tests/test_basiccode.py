
import pytest
from src import basiccode
import boto3
import os
from moto import mock_s3

BUCKET_NAME = "test"
S3_SERVICE = "s3"
REGION = "us-east-1"
os.environ['LOCALSTACK_HOSTNAME'] = 'localhost'

"""mock S3 client to test the get object file from s3 bucket"""
@pytest.fixture
def mock_s3_client():
    with mock_s3():
        s3_client = boto3.client(S3_SERVICE, region_name=REGION)
        yield s3_client


dynamodb = boto3.client(service_name='dynamodb',
                        endpoint_url='http://localhost:4566')
table_name = 'mytable'
try:
    #"Test the write_into_table with a valid input data"
    table = dynamodb.create_table(TableName=table_name,
                                  KeySchema=[
                                      {'AttributeName': 'rows', 'KeyType': 'HASH'}],
                                  AttributeDefinitions=[
                                      {'AttributeName': 'rows', 'AttributeType': 'N'}],
                                  ProvisionedThroughput={
                                      'ReadCapacityUnits': 10,
                                      'WriteCapacityUnits': 10
                                  }
                                  )
except dynamodb.exceptions.ResourceInUseException:
    dynamodb_source = boto3.resource(
        service_name='dynamodb', endpoint_url='http://localhost:4566')
    table = dynamodb_source.Table(table_name)

"""test read file from s3 using s3 mock client"""
@pytest.mark.parametrize(
    "object_key, content",
    [
        ("test.csv", "this is content"),
        ("test.xlsx", "this is content")
    ]
)
def test_read_file_from_s3(mock_s3_client, object_key, content):
    #s3_client = boto3.resource(S3_SERVICE, region_name=REGION)
    mock_s3_client.create_bucket(Bucket=BUCKET_NAME)
    mock_s3_client.put_object(Bucket=BUCKET_NAME, Key=object_key, Body=content)
    data = basiccode.read_file_from_s3(mock_s3_client, BUCKET_NAME, object_key)
    assert data == content


"""test insert data into dynamodb"""
@pytest.mark.parametrize(
    "dynamodb, count_row, expected",
    [
        (
            boto3.resource(service_name="dynamodb",
                           endpoint_url='http://localhost:4566'),
            9,
            200
        ),
        (
            "",
            9,
            ValueError
        )
    ]
)
def test_insert_data(dynamodb, count_row, expected):
    assert basiccode.insert_row_to_dynamodb(dynamodb, count_row) == expected


"""test count row from csv file function"""
@pytest.mark.parametrize(
    "data, expectData",
    [
        ("csv_test/test.csv", 23),
        ("csv_test/test1.csv", 21),
        ("csv_test/test2.csv", 1),
        ("csv_test/test3.csv", 0)
    ]
)
def test_count_row_data(data, expectData):
    with open(data) as csv_file:
        assert basiccode.count_row_data(csv_file) == expectData
