resource "aws_lambda_event_source_mapping" "count_row" {
  event_source_arn  = aws_dynamodb_table.mytable.stream_arn
  function_name     = aws_lambda_function.test_lambda.arn
  starting_position = "LATEST"
}
