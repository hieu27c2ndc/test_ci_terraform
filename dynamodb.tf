resource "aws_dynamodb_table" "mytable" {
  name             = "mytable"
  billing_mode     = "PROVISIONED"
  read_capacity    = "5"
  write_capacity   = "5"
  stream_enabled   = true
  stream_view_type = "NEW_IMAGE"
  hash_key         = "rows"


  attribute {
    name = "rows"
    type = "N"
  }

  replica {
    region_name = "us-east-1"
  }


  tags = {
    Name        = "dynamodb-test-table"
    Environment = "dev"
  }
}

output "dynamo_arn" {
  value       = aws_dynamodb_table.mytable
  description = "Mock"
}
