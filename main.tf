provider "aws" {
  access_key                  = "mock"
  secret_key                  = "mock"
  region                      = "us-east-1"
  s3_use_path_style           = true
  skip_credentials_validation = true
  skip_metadata_api_check     = true
  skip_requesting_account_id  = true

  endpoints {
    dynamodb = "http://127.0.0.1:4566"
    iam      = "http://127.0.0.1:4566"
    lambda   = "http://127.0.0.1:4566"
    s3       = "http://127.0.0.1:4566"
  }
}
